"War" card game simulation.
Command-line interface provides info on the status and stats of the game.
2 players, 52 cars
Can be used as benchmark as games per second varies from system to system.
Great tool to use instead of coin tossing.