package Zadanie22_Wojna;

public class GameStats {
    private double totalDuration = 0;
    private long totalGameCount = 0;
    private double averageDuration;
    private long totalTurns = 0;
    private int minTurns = Integer.MAX_VALUE;
    private int maxTurns = Integer.MIN_VALUE;
    private double averageTurns;

    public void updateStats(GameResults results){
        totalDuration += results.duration;
        totalGameCount++;
        totalTurns += results.stageCount;
        averageDuration = totalDuration / totalGameCount;
        averageTurns= (double) totalTurns / (double) totalGameCount;
        if (minTurns > results.stageCount) minTurns = results.stageCount;
        if (maxTurns < results.stageCount) maxTurns = results.stageCount;
    }

    public void printStats(){
        System.out.printf("\n%24s %5f \n", "Average game duration:", averageDuration);
        System.out.printf("%24s %f\n", "Average length in turns:", averageTurns);
        System.out.printf("%24s %d\n", "Total game count:", totalGameCount);
        System.out.printf("%24s %d\n", "Minimum turns:", minTurns);
        System.out.printf("%24s %d\n", "Maximum turns:", maxTurns);
    }

    public long getTotalGameCount() {
        return totalGameCount;
    }
}
