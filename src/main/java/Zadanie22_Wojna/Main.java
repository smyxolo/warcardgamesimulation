package Zadanie22_Wojna;

import java.util.Scanner;
import java.util.Timer;

public class Main {
        private static String player1Name;
        private static String player2Name;
        private static Integer gamesToPlay = null;

    public static void main(String[] args) {
        getGameData();
        GameManager gm = new GameManager(player1Name, player2Name);
        Timer timer = new Timer();
        timer.schedule(gm.printStats, 500, 2000);
        gm.runGame(gamesToPlay);
        timer.cancel();
    }

    private static void getGameData(){
        Scanner scan = new Scanner(System.in);
        //get player names and amount of games to play
        System.out.println("Enter Player1 name:");
        player1Name = scan.nextLine();
        System.out.println("Enter Player2 name:");
        player2Name = scan.nextLine();
        System.out.println("Enter Number of games to perform: ");
        System.out.println("Enter negative number for infinite play.");
        while (gamesToPlay == null) {
            try {
                gamesToPlay = Integer.parseInt(scan.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Incorrect number format");
            }
        }
    }
}
