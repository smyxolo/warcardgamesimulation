package Zadanie22_Wojna.GameElements;

import java.util.ArrayList;

public class Player {
    private String name;
    public int currentCardValue;
    public int gamesWon = 0;
    public ArrayList<Card> playerCardList = new ArrayList<>();

    public Player(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void clearDeck(){
        playerCardList = new ArrayList<>();
    }
}
