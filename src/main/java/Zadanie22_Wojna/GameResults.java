package Zadanie22_Wojna;

import Zadanie22_Wojna.GameElements.Player;

import java.time.Duration;
import java.time.LocalDateTime;

public class GameResults {

    protected Player winner;
    protected int stageCount = 0;
    protected int warCount = 0;
    protected LocalDateTime start;
    protected LocalDateTime finish;
    protected double duration;

    public void updateDuration(){
        duration = Duration.between(start, finish).toMillis();
    }

    public void setWinner(Player winner) {
        this.winner = winner;
    }

    public void resetForNewGame(){
        stageCount = 0;
        warCount = 0;
    }
}
